FROM openjdk:12-alpine

EXPOSE 8761

ADD ./target/*.jar app.jar
 
ENTRYPOINT ["java","-jar","/app.jar"]
